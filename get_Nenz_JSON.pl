#!/usr/bin/perl

use Socket;
use FileHandle;
use MIME::Base64;
use JSON;


#################################################################
#  file1, fiel2 and fiel3 are Enzyme data                       #
#  file4 is an input data:                                      #
#       mixed with GlycoCT (condensed),                         #
#       KCF and IUPAC is available                              #
#       *a blank line is necessary at the end of each GlycoCT   #
#################################################################

$file1 = "EnzymeList/Nenz-Add_Sub.txt";
$file2 = "EnzymeList/Nenz-Pro.txt";
$file3 = "EnzymeList/Nenz-Comm.txt";
$file4 = $ARGV[0];

@enz_as= ();		#data from file1
%enz_pr = ();		#data from file2
%enz_cm = ();		#data from file3
%jsonOUT = ();		#key:IUPAC Structure, val:Array of Addictive node, Substrate & Enzyme Info 
$Y = 0;





#################################################################
#  file1 contains a list of :                                   #
#       [0] Enzyme ID (for this work),                          #
#       [1] Addition Node & Edge,                               #
#       [2] Substrate (where the addtion node connected to)     #
#         (& Edge, if there)                                    #
#                                                               #
#  @enz_as contains arrays of index [0] to [2] (above).         #
#################################################################
open(FILE, $file1);
while(<FILE>){
  my $line = $_; 
  chomp ($line);
  $Y += 1;
  $line =~ s/^\t//g;
  $line =~ s/\t\t/\t/g;
  
  my @tmp = split (/\t/, $line);
  push(@enz_as,\@tmp);
}
close(FILE);



#################################################################
#  file2 contains a list of :                                   #
#       [0] Enzyme ID (for this work)                           #
#       [1] Parent1 Node (& Edge)                               #
#       [2] Child1 Node (& Edge)                                #
#       [3] Child2 Node (& Edge)                                #
#       [4] Child3 Node (& Edge)                                #
#                                                               #
#  *if a parent has only one (or two) child(ren),               #
#   left child-index is represented with "NA"                   #
#  *[1]-[4] indexes continue 4 times (until Parent4)            #
#                                                               #
#  %enz_pr: key->Enzyme ID (for this work)                      #
#           val->%tmp_pr                                        #
#  %tmp_pr: key->Parent Node                                    #
#           val->array of its children                          #
#################################################################
open(FILE, $file2);
while(<FILE>){
  my $line = $_; 
  chomp ($line);
  $line =~ s/^\t//g;
  $line =~ s/\t\t/\t/g;

  my @tmp = split (/\t/, $line);
  my $prK = shift(@tmp);
  
  my $tmp_pr=&GetPaCh(\@tmp);
  $enz_pr{$prK} = $tmp_pr;
}
close(FILE);




#################################################################
#  file3 contains a list of :                                   #
#       [0] Enzyme ID (for this work)                           #
#       [1] Recommended Protain Name (from UniProt or NCBI)	#
#       [2] Recommended Gene Name (from UniProt or NCBI)	#
#       [3] Resource URL (UniProt or NCBI)			#
#       [4] Localization (from UniProt or BRENDA)		#
#       [5] EC number						#
#       [6-15] JCGGDB						#
#       [16-25] KEGG						#
#       [26-27] BRENDA						#
#       [28-37] CFG						#
#       [38-39] CAZY						#
#       [40-51] NCBI						#
#                                                               #
#  %enz_cm: key->Enzyme ID (for this work)                      #
#           val->an array of [1]-[51]                           #
#								#
#  If there are multiple protain/gene names, 			#
#     		           they can be separated with '","'.	#
#								#
#  For instanse, Enzyme ID = 3					#
#  		 Gene Names are = "B4GALT1","B4GALT2"		#
#################################################################
open(FILE, $file3);
while(<FILE>){
  my $line = $_; 
  chomp ($line);
  $Y += 1;
  $line =~ s/^\t//g;
  $line =~ s/\t\t/\t/g;
  
  my @tmp = split (/\t/, $line);
  my $cmK = shift(@tmp);
  $enz_cm{$cmK} = \@tmp;
}
close(FILE);





#################################
#  Main				#
#################################
my $kcf = "";
my $glyct = "";
my $kmode = -1;
my $gmode = -1;
my $countIN = 0;

open(FILE, $file4);
while(<FILE>){
  my $line = $_; 
  $countIN += 1;
  
  ###############################################
  #  Read file4: GlycoCT (Condensed)            #
  #     Transform GlycoCT to KCF, and then      #
  #     transform KCF to IUPAC                  #
  #  * A "Fuzzy" structure will be skipped      #
  ###############################################
  if($line =~ /^RES/ && $gmode == -1){
     $glyct = $line;
     $gmode = 1;
     $countG += 1;
     next;
  }elsif($line =~ /^\s*\n/ && $gmode == 1){
     $glyct .= "\n";

     my $exclude = "AEP|NbOHC|Ara|Alt|UND|-[dl]thr-|-[dl]ara-|-[dl]alt-|x-HEX-x:x";
     if($glyct =~ /($exclude)/){
        print STDERR "\n\nSTDERR 13) INPUT data includes non-monosaccharide moiety or unacceptable moiety for transrating into KCF nor IUPAC format: $1\n";
        $glyct = "";
        next;
     }elsif($glyct =~ /UND/){
        print STDERR "\n\nSTDERR 14) INPUT data includes Fuzzy moiety -  UND section is unacceptable.\n";
        $glyct = "";
        next;
     }elsif($glyct =~ /\(-1\+\d\)/){
        print STDERR "\n\nSTDERR 15) INPUT data includes Fuzzy moiety -";
        print STDERR "\n \"-1\" is unacceptable as linkage information.\n$glyct\n\n";
        $glyct = "";
        next;
     }else{
        my $glyct_check = $glyct;
        my @glct = split(/\n/,$glyct_check);
        my $glsize = @glct;

        if($glsize == 5 && $glct[2] =~ /^2s/){
           print STDERR "\n\nSTDERR 16) INPUT data is monosaccharide+ $glyct\n\n";
           $glyct = "";
           next;
        }
     }

     
     $line = &TransGtoK($countIN, $glyct) if ($glyct ne "");
     $glyct = "";
     $kcf = "";
     $gmode = -1;
     $kmode = 1;
  }elsif($line !~ /^\n/ && $gmode == 1){ 
     $glyct .= $line;
     next;
  }



  ###############################################
  #  Read file4: KCF                            #
  #     Transform KCF to IUPAC                  #
  ###############################################
  if($line =~ /^ENTRY/ && $kmode == -1){
     $kcf = $line;
     $kmode = 1;
     $countK += 1;
     next;
  }elsif($line =~ /\/\/\// && $kmode == 1){
     $kcf .= $line;
     $line = &TransKtoI($countIN, $kcf);
     $kcf = "";
     $kmode = -1;
  }elsif($line !~ /\/\/\// && $kmode == 1){ 
     $kcf .= $line;
     next;
  }


  #######################################################################
  #  Search glycosyltransferases based on an IUPAC structure by:	#
  #									#
  #	1) make two arrays (parent-children, and child-parent relation) #
  #	2) search enzyme info based on					#
  #		a child (=addactive) and a parent (=substrate)		#
  #	3) check the rule (constraint)					#
  #	4) store in json format						#
  #									#
  #######################################################################
  chomp ($line);
  $line =~ s/\s//g;
  $line =~ s/\[/ [ /g;
  $line =~ s/\]/ ] /g;
  $line =~ s/\)/) /g;
  $line =~ s/\s+/ /g;

  $jsonOUT{$line} = ();

  my @iupac = split(/\s/, $line);
  my $iupsize = @iupac;
  my %prchHash = ();		#key->A parent, val->An array of child(ren)
  my %chprHash = ();		#key->A child, val->A parent: A parent & a child represent "Node Num - monosaccharide (and linkage)".
  my $node = 1;


  #######################################################################
  #  1) make two hashes (parent-children, and child-parent relation) 	#
  #######################################################################
  foreach (my $i=0; $i<$iupsize-1; $i++){
     if($iupac[$i] =~ /.+\(.+\)/){
	my $child = $node."_".$iupac[$i];
     
        if ($iupac[$i+1] =~ /([^\[\]\(\)]+)(\(.+\))?/){
	   my $tmpNode = $node+1;
	   my $parent = $tmpNode."_".$iupac[$i+1];
	   my $tmpHash = &GetHash(\%prchHash, $child, $parent);
	   %prchHash = %{$tmpHash};
	   $chprHash{$child} = $parent;
	   $node += 1;
        }elsif($iupac[$i+1] =~ /\]/ || $iupac[$i+1] =~ /\[/){
   	   my $bb = 0;
	   my $eb = 0;
	   my $tmpNode = 0;
           if($iupac[$i+1] =~ /\[/){
	      $bb = 1;
           }

	   for (my $j=$i+2; $j<$iupsize; $j++){
	      if($iupac[$j]=~/\[/){
	         $bb += 1;
	      }elsif($iupac[$j]=~/\]/){
	         $eb += 1;
	      }elsif($iupac[$j] =~ /([^\[\]\(\)]+)(\(.+\))?/){
		 $tmpNode += 1;
	         if($bb == $eb){
	            $tmpNode = $tmpNode+$node;
	   	    my $parent = $tmpNode."_".$iupac[$j];
	 	    my $tmpHash = &GetHash(\%prchHash, $child, $parent);
		    %prchHash = %{$tmpHash};
	            $chprHash{$child} = $parent;
		    $node += 1;
		    last;
		 }
	      }
	   }
	}
     }
  }


  #######################################################################
  #	2) search enzyme info based on					#
  #		a child (=addactive) and a parent (=substrate)		#
  #######################################################################
  foreach my $tmpA (sort keys %chprHash){
    for (my $i=0; $i<@enz_as; $i++){
       my $adsug = "";
       my $adlin = "";
       my $ezAsug = "";
       my $ezAlin = "";
       my $sbsug = "";
       my $sblin = "";
       my $ezSsug = "";
       my $ezSlin = "";
       my $constcheck = 0;

       if($tmpA =~ /^\d+_([a-zA-Z5]+)([\?\(]?[?ab12]{0,2}[\?\-]?[\?\d]?\)?)$/){
          $adsug = $1;
          $adlin = $2;
          if($adsug =~ /Neu5Ac/){
             $adsug = "NeuAc";
          }
       }

       my $ezNum = ${$enz_as[$i]}[0];
       my $ezAdd = ${$enz_as[$i]}[1];
       my $ezSub = ${$enz_as[$i]}[2];

       if($ezAdd =~ /^([a-zA-Z]+)(\([ab][12]\-\d\))$/){
          $ezAsug = $1;
          $ezAlin = $2;
       }

       if($ezSub =~ /^[a-zA-Z]+$/){
          $ezSsug = $ezSub;
       }elsif($ezSub =~ /^([a-zA-Z]+)(\([ab][12]\-\d?\)?)$/){
          $ezSsug = $1;
          $ezSlin = $2;
       }



       ##################################################################
       #	3) check the rule (constraint)				#
       ##################################################################
       if($ezAsug eq $adsug){
          if($ezAlin eq ""){
	     $constcheck = 1;
	  }elsif($adlin eq ""){
	     $constcheck = 1;
	  }elsif($ezAlin eq $adlin){
	     $constcheck = 1;
	  }else{
             my $coch = &CompLinks($adlin, $ezAlin);
	     $constcheck = 1 if ($coch == 1);
	  }
       }

       if($constcheck == 1){
          if($chprHash{$tmpA} =~ /^\d+_([a-zA-Z5]+)(\(?[\?ab]?[\?12]?\-?[\?\d]?\)?)$/){
             $sbsug = $1;
             $sblin = $2;
             if($sbsug =~ /Neu5Ac/){
                $sbsug = "NeuAc";
             }
          }

          if($ezSsug eq $sbsug){
	     if($ezSlin eq ""){
	        $constcheck = 2;
	     }elsif($sblin eq ""){
	        $constcheck = 2;
	     }elsif($ezSlin eq $sblin){
	        $constcheck = 2;
	     }else{
                my $coch = &CompLinks($sblin, $ezSlin);
	        $constcheck = 2 if ($coch == 1);
	     }
	  }
       }

       if($constcheck == 2){
          my %ezPro = %{$enz_pr{$ezNum}};
          my @ezProP = keys(%ezPro);
          my $sug =~ qr/[^\[\]\(\)]+/;
          my $lin =~ qr/\(?[ab\d-]*\)?/;
          my ($sugproP, $linproP);

          foreach my $proP (@ezProP){
             if($proP =~ /($sug)($lin)?/){
                $sugproP = $1;
                $linproP = $2;
             }
             foreach my $iupP (keys(%prchHash)){
                if($iupP =~ /($sug)($lin)?$/ && $1 eq $sugproP){
                   my $coch = &CompLinks($2, $linproP);
                   $constcheck=-1 if ($coch == -1);

                   if($constcheck == 2){
                      foreach my $ezproC (@{$ezPro{$proP}}){
                         if($ezproC =~ /($sug)($lin)?/){
                            $sugproC = $1;
                            $linproC = $2;
                         }
                         foreach my $iupC (@{$prchHash{$iupP}}){
                            if($iupC =~ /($sug)($lin)?$/ && $1 eq $sugproC){
                               my $coch = &CompLinks($2, $linproC);
                               $constcheck=-1 if ($coch == -1);
                            }
                         }
                      }
                   }
                }
             }
          }
       }

       if($constcheck == 2){
	  my $checkV = 1;
          if($ezID==26 || $ezID==27 || $ezID==29 || $ezID==30 || $ezID==31 || $ezID == 28 || $ezID == 32){
	     $checkV = &CompInPr($ezNum, $tmpA, \%chprHash, $line);
	  }


          #######################################################################
          #   4) store in json format                                           #
          #      @cm[0]UniProtKB Recommended protein name,                      #
          #         [1]UniProtKB gene name, [2]UniProtKB URL,                   #
          #         [3]Enzyme localization, [4]EC number                        #
          #######################################################################
	  if($checkV == 1){
	     my @cm = @{$enz_cm{$ezNum}};
	     my @tmpV = ($tmpA,$chprHash{$tmpA},$cm[0],$cm[1],$cm[2],$cm[3],$cm[4]);
	     push(@{$jsonOUT{$line}},\@tmpV);
	  }
       }
    }
  }



#########################################
#  Output: json file			#
#########################################
   my $json_out = JSON->new->encode(\%jsonOUT);

   @record = split/\]\]/,$json_out;
   foreach(@record){
      @e = split/\:\[/,$_;

      $forStr = @e[0];
      $forStr =~ tr/ //ds;
      $forStr =~ tr/\"||\,//ds;
      $forStr =~ s/^\{//;
      $forStr =~ s/\}$//;

      $size = @e;
      if($size > 2) {
         print "warning\n\n -----> @e[0]";
         exit;
      }
      if($forStr !~ /null/){
         foreach(@e[1]) {
            @comp = split/\]\,\[/,$_;
            @unit = ();

            foreach(@comp){
               $_ =~ s/\]\]//g;
               $_ =~ s/\[\[//g;
               $_ =~ s/\[//g;

               @component = split/\",\"/,$_;

               foreach(@component){
                  $_ =~ s/\\"/"/g;
                  $_ =~ s/^"||"$//g;
                  $_ =~ s/\",\"/<BR>/g;
               }

               push(@unit,{unit1=>@component[0],unit2=>@component[1],protein=>@component[2],gene=>@component[3],url=>@component[4],location=>@component[5],enz=>@component[6]});
	       #[0]Adductive monosaccharide, [1]Substrate monosaccharide,
	       #[2]UniProtKB Recommended protein name, [3]UniProtKB gene name, [4]UniProtKB URL, [5]Enzyme localization, [6]EC number
            }
         }
      }


      if($forStr ne "" && $forStr !~ /null/){
         $write = encode_json( { str=>$forStr,  parts=> \@unit } ) . "\n";
         $fileout = "/tmp/".$forStr.".json";
         open(OUT, ">$fileout");
            print OUT $write;
         close(OUT);
      }
   }
   $json_out = "";
   %jsonOUT = ();
}
close(FILE);







#================================================================#
#################################
#	Subroutins		#
#################################

sub CompInPr {
  my ($ezID, $adduct, $tmpCPh, $constiup) = @_;
  my %cpHash = %{$tmpCPh};
  my $check = 1;
  my $subst = $cpHash{$adduct};

  if($ezID==26 || $ezID==27 || $ezID==29 || $ezID==30){
     my $moiety = $subst;
     my $granP = $cpHash{$moiety};
     my $countP = 2;

     while(exists($cpHash{$moiety})){
        $countP += 1;
        my $granP = $cpHash{$moiety};
        $moiety = $granP;
     }
     foreach my $bischeck (keys(%cpHash)){
        if($bischeck =~ /GlcNac\(b1\-4\)/ && $cpHash{$bischeck} =~ /Man\(b1\-4\)/){
           if($countP > 6){
              $check = -1;
              return $check;
           }
        }
     }
  }elsif($ezID==31){
     my $moiety = $subst;
     my $granP = $cpHash{$moiety};

     while(exists($cpHash{$moiety})){
        $countP += 1;
        my $granP = $cpHash{$moiety};
        if($granP =~ /Gal\(b1\-/){
           $check = -1;
           return $check;
        }
        if($granP =~ /Man\(a1\-/){
           $check = -1;
           return $check;
        }
        $moiety = $granP;
     }
  }elsif($ezID == 28){
     my $granP = $cpHash{$subst};
     if($granP eq ""){
        $check = 1;
     }elsif($granP =~ /Man\(?[a\?]?[1\?]?\-?[3\?]?\)?/){
        my $grangranP = $cpHash{$granP};
        if($grangranP =~ /Man\(?[b\?]?[1\?]?\-?[4\?]?\)?/){
           $check = -1;
        }elsif($grangranP eq ""){
           $check = 1;
        }
     }
  }elsif($ezID == 32){
     my $moiety = $subst;
     my $granP = $cpHash{$moiety};
     my $countP = 2;

     while(exists($cpHash{$moiety})){
        $countP += 1;
        my $granP = $cpHash{$moiety};
        if($granP =~ /Gal\(b1-/){
           $check = -1;
           return $check;
        }elsif($granP =~ /GlcNAc\(?[b\?]?[1\?]?\-?[2\?]?\)?/){
           my $grangranP = $cpHash{$granP};
           if($grangranP =~ /Man\(?[a\?]?[1\?]?\-?[3\?]?\)?/){
              $check = 1;
           }
        }
        $moiety = $granP;
     }

     foreach my $bischeck (keys(%cpHash)){
        if($bischeck =~ /GlcNac\(b1-4\)/ && $cpHash{$bischeck} =~ /Man\(b1\-4\)/){
           if($countP > 5){
              $check = -1;
              return $check;
           }
        }
     }
  }
  return $check;
}






sub CompLinks {
  my ($inplin, $enzlin) = @_; 
  my $eab = "";
  my $eot = "";
  my $ete = "";
  my $check = -1;
  if($enzlin =~ /\(([ab])([12])\-(\d?)/){
     $eab = $1;
     $eot = $2;
     $ete = $3;
  }


  if($inplin =~ /\?/){
     if($inplin =~ /([ab])([12])\-\??\)/){
        my $aab = $1;
        my $aot = $2;
        if($aab eq $eab && $aot == $eot){
      	   $check = 1;
        }
	
     }elsif($inplin =~ /([ab])\??\-(\d)/){
        my $aab = $1;
        my $ate = $2;
        if($aab eq $eab && $ate == $ete){
   	   $check = 1;
        }elsif($aab eq $eab && $ete eq ""){
   	   $check = 1;
        }

     }elsif($inplin =~ /\??([12])-(\d)/){
        my $aot = $1;
        my $ate = $2;
        if($aot == $eot && $ate == $ete){
   	   $check = 1;
        }elsif($aot eq $eot && $ete eq ""){
   	   $check = 1;
        }

     }elsif($inplin =~ /([ab])\??-\??/){
        my $aab = $1;
        if($aab eq $eab){
   	   $check = 1;
        }

     }elsif($inplin =~ /\??([12])\-\??/){
        my $aot = $1;
        if($aot == $eot){
   	   $check = 1;
        }

     }elsif($inplin =~ /\??\??\-(\d)/){
        my $ate = $1;
        if($ate == $ete){
   	   $check = 1;
        }

     }elsif($inplin =~ /\??\??-\??/){
        $check = 1;
     }
  
  }elsif($inplin =~ /([ab])([12])\-(\d)/){
     my $aab = $1;
     my $aot = $2;
     my $ate = $3;

     if($aab eq $eab && $aot == $eot && $ate == $ete){
   	$check = 1;
     }elsif($aab eq $eab && $aot == $eot && $ete eq ""){
   	$check = 1;
     }
  }
  return($check);
}





sub GetPaCh {
  my $enz = @_;
  my @enzline = @{$enz};
  my $numEnz = @enzline;
  my @tmpEnz= ();
  my $na = "NA";

  if($numEnz == 16){
     my $item = 0;
     my %tmpH = ();
     while($item < 13){
        my @tmpA = ();
	if($enzline[$item] =~ /^NA$/){
	   last;
        }else{
   	   push(@tmpA, $enzline[$item+1]);
	   if($enzline[$item+2] ne $na){
	      push(@tmpA, $enzline[$item+2]);
	      if($enzline[$item+3] ne $na){
   	   	 push(@tmpA, $enzline[$item+3]);
	      }
	   }
           $tmpH{$enzline[$item]} = \@tmpA;
	}
	$item += 4;
     }
     return(\%tmpH);
  }
}




sub GetHash{
  my ($hash, $child, $parent) = @_;
  my %pcHash = %{$hash};

  if(exists $pcHash{$parent}){
     push(@{$pcHash{$parent}},$child);
  }else{
     $pcHash{$parent} = [$child];
  }
  return(\%pcHash);
}






sub TransKtoI{
   my ($number, $otherIN) = @_;

   my $mode = -1;
   my $nodeNum = 0;
   my $edgeNum = 0;
   my (%chpr, %prch, %node, %edge, $item);

   my @tmpKCF = split (/\n/, $otherIN);
   
   foreach my $line (@tmpKCF){
      chomp ($line);

      if($line =~ /^ENTRY/){
         $mode = 1;
      }elsif($line =~ /^NODE\s+(\d+)/){
         $nodeNum = $1;
         $mode = 2;
      }elsif($line =~ /^EDGE\s+(\d+)/){
         $edgeNum = $1;
         $mode = 3;
      }elsif($line =~ /^\/\/\//){
         $mode = 4;
      }

      if($line =~ /\s+(\d+)\s+(\S+)\s+/ && $mode == 2){
         my $nodeID = $1;
         my $nodeName = $2;
         $node{$nodeID} = $nodeName;
      }elsif($line =~ /\s+\d+\s+(\d+:?[ab]?[12]?)\s+(\d+:?\d?)\s*$/ && $mode == 3){
         my $childful = $1;
         my $parentful = $2;
         my ($childNum, $parentNum, $redu, $nonredu);
         my $checkEdge = 0;
         if($childful =~ /^(\d+):([ab][12])$/){
            $childNum = $1;
            $redu = $2."-";
            $checkEdge += 1;
         }elsif($childful =~ /^(\d+):([12])$/){
            $childNum = $1;
            $redu = "?".$2."-";
            $checkEdge += 1;
         }elsif($childful =~ /^(\d+):([ab])$/){
            $childNum = $1;
            $redu = $2."?-";
            $checkEdge += 1;
         }elsif($childful =~ /^\d+$/){
            $childNum = $childful;
            $redu = "??-";
            $checkEdge += 1;
         }

         if($parentful =~ /^(\d+):(\d)$/){
            $parentNum = $1;
            $nonredu = $2;
            $checkEdge += 1;
         }elsif($parentful =~ /^\d+$/){
            $parentNum = $parentful;
            $nonredu = "?";
            $checkEdge += 1;
         }

         if($checkEdge == 2){
            my $linkage = $redu.$nonredu;
            $edge{$childNum} = $linkage;
            $chpr{$childNum} = $parentNum;
            if(exists $prch{$parentNum}){
               push(@{$prch{$parentNum}}, $childNum);
            }else{
               my @array = ($childNum);
               $prch{$parentNum} = \@array;
            }
         }else{
            print STDERR "\n\nSTDERR 1) ERROR at EDGE section.\nINPUT FILE: line $number\n";
            exit;
         }
      }elsif(scalar(keys(%node)) == $nodeNum && scalar(keys(%edge)) == $edgeNum && $mode == 4){
         my %countC = ();
         foreach my $item (@node){
            $countC{$item} = 0;
         }

         my $max = 0;
         my $root = 0;
         foreach my $item (keys(%prch)){
            my $num = 0;
            $num = &CountChildren(\%prch, $item, $num);
            $countC{$item} = $num;
            if ($max < $num){
               $max = $num;
               $root = $item;
            }
         }
   
         my @result = ();
         my $res = &makeIUP($root, \%countC, \%prch, \@result, -1);
         @result = reverse(@{$res});

         for (my $i=0; $i<@result; $i++){
            if($result[$i] =~ /[\[\]]/){
               $item .= $result[$i];
            }else{
               $item .= $node{$result[$i]};
               if(exists $edge{$result[$i]}){
                  $item .= "(".$edge{$result[$i]}.")";
               }
	    }
         }
      }
   }
   return ($item);
}




sub CountChildren{
   my ($pc, $parent, $number) = @_;
   my %parchi= %{$pc};

   if(exists $parchi{$parent}){
      $number += @{$parchi{$parent}};
      foreach my $kid (@{$parchi{$parent}}){
         $number = &CountChildren(\%parchi, $kid, $number);
      }
   }
   return ($number);
}




sub makeIUP{
   my ($parent, $cc, $pc, $ra, $pIndex) = @_;
   my %countChild = %{$cc};
   my %parchi = %{$pc};
   my @resultAr = @{$ra};
   my @tmpAr = ();

   if($pIndex == -2){
      return(\@resultAr);
   }elsif($pIndex == -1){
      push(@tmpAr, $parent);
   }else{
      my @array = @{$parchi{$parent}};
      if(@array == 1){
         push(@tmpAr, $array[0]);
      }elsif(@array == 2){
         my $num1 = $countChild{$array[0]};
         my $num2 = $countChild{$array[1]};
         push(@tmpAr, "]");
         if($num1 < $num2){
            push(@tmpAr, $array[0]);
            push(@tmpAr, "[");
            push(@tmpAr, $array[1]);
         }else{
            push(@tmpAr, $array[1]);
            push(@tmpAr, "[");
            push(@tmpAr, $array[0]);
         }
      }elsif(@array == 3){
         my $num1 = $countChild{$array[0]};
         my $num2 = $countChild{$array[1]};
         my $num3 = $countChild{$array[2]};
         push(@tmpAr, "]");
         push(@tmpAr, $array[0]);
         push(@tmpAr, "[");
         push(@tmpAr, "]");
         push(@tmpAr, $array[1]);
         push(@tmpAr, "[");
         push(@tmpAr, $array[2]);
      }elsif(@array == 4){
         my $num1 = $countChild{$array[0]};
         my $num2 = $countChild{$array[1]};
         my $num3 = $countChild{$array[2]};
         my $num4 = $countChild{$array[3]};
         push(@tmpAr, "]");
         push(@tmpAr, $array[0]);
         push(@tmpAr, "[");
         push(@tmpAr, "]");
         push(@tmpAr, $array[1]);
         push(@tmpAr, "[");
         push(@tmpAr, "]");
         push(@tmpAr, $array[2]);
         push(@tmpAr, "[");
         push(@tmpAr, $array[3]);
      }
   }
   splice(@resultAr, $pIndex+1, 0, @tmpAr);

   for(my $i=0; $i<@tmpAr; $i++){
      $parent = $tmpAr[$i];
      for(my $j=0; $j<@resultAr; $j++){
         if($resultAr[$j] == $parent && $parent =~ /\d/){
            $pIndex = $j;
            last;
         }else{
            $pIndex = -2;
         }
      }
      if($pIndex == -1){
         print STDERR "STDERR 2) ERROR!: residue $parent is not stored as parent\n";
         exit;
      }else{
         my $res = &makeIUP($parent, \%countChild, \%parchi, \@resultAr, $pIndex);
         @resultAr = @{$res};
      }
   }

   return (\@resultAr);
}






sub TransGtoK{
   my ($number, $otherIN) = @_;
   my $file_nodes = "transNODEs_uniq.txt";  #unicarb condense file

   my %uniqNODE = ();
   open(IN, $file_nodes);
   while (<IN>){
      my $line = $_;
      if($line =~ /^([\S]+)\s+([\S]+)\n$/){
         $uniqNODE{$1} = $2;
      }
   }

   my $mode = -1;
   my (%ress, %lins, %carbs, $unicarbs, $kcfs, $disaps, $uniLINES);
   my $numKCF = 1;
   my $numUNI = 1;
   my $numIN = 0;

   my @tmpKCF = split (/\n/, $otherIN);
   
   foreach my $line (@tmpKCF){
      $uniLINES .= $line;
      $number += 1;

      if($line =~ /RES/ && $mode == -1){
         $mode = 1;
         $numIN += 1;
      }elsif($line =~ /LIN/ && $mode == 1){
         $mode = 2;
      }elsif($line =~ /(UND|ALT|REP)/ && $mode == 2){
         $mode = 3;
      }elsif($line =~ /^\n$/ && $mode == 3){
         $disaps .= $uniLINES."\n";
         $mode = -1;
         (%ress, %lins, %carbs) = ();
         $uniLINES = "";
         $numUNI += 1;
      }

      if($line =~ /^(\d+[bs]):(.+)$/ && $mode == 1){
         $ress{$1} = $2;
      }elsif($line =~ /^\d+[^bs]:.+$/ && $mode == 1){
         print STDERR "STDERR 3) Check! There is another letter out of b and s at RES section\n$line";
         print STDERR "<input data> line around (before): $number\n";
         exit;
      }elsif($line =~ /^(\d+):(\d+d\(.+\)\d+n)$/ && $mode == 2){
         $carbs{$1} = $2;
      }elsif($line =~ /^(\d+):(\d+o\(.+\)\d+[dn])$/ && $mode == 2){
         $lins{$1} = $2;
      }
   }
   if($mode != 3){
      $kcfs .=  &main(\%ress, \%carbs, \%uniqNODE, \%lins, $numKCF, $number);
      $mode = -1;
      (%ress, %lins, %carbs) = ();
      $uniLINES = "";
      $numKCF += 1;
   }


   if($disaps ne ""){
      print OUT "There is unavailable stucture for transforming GlycoCT to KCF\n";
      print OUT $disaps;
      print OUT "\n\n";
   }

   return ($kcfs);
}




sub main{
   my ($ressREF, $carbsREF, $uniqNODEREF, $linsREF, $numkcf, $number) = @_;
   my ($pre_nodesREF, $change_nidsREF, $nodeREF, $edgeREF, $rootnode, $oyakoREF, $xHashREF, $yHashREF, $habaHashREF, $kcf);
   ($pre_nodesREF, $change_nidsREF, $uniqNODEREF) = &getKCFnodes($ressREF,$carbsREF,$uniqNODEREF);

   ($nodeREF, $edgeREF, $rootnode, $oyakoREF, $nodedgesREF) = &makeHashes4XY($pre_nodesREF,$change_nidsREF,$linsREF, $number);

   my %xHash = ();
   my %yHash = ();
   my %habaHash = ();
   $xHash{$rootnode} = 0;
   $yHash{$rootnode} = 0;

   ($oyakoREF, $nodeREF, $xHashREF, $habaHashREF) = &getX($rootnode, $oyakoREF, $nodeREF, \%xHash, \%habaHash);
   my ($id, $oyakoRef, $nodesRef, $xHashRef, $habaHashRef) = @_;
   ($oyakoREF, $nodeREF, $yHashREF, $habaHashREF) = &getY($rootnode, $oyakoREF, $nodeREF, \%yHash, $habaHashREF, $nodedgeREF);


   $kcf = &makeKCF($rootnode, $nodeREF, $edgeREF, $xHashREF, $yHashREF, $numkcf);
   return $kcf;
}




sub getKCFnodes{
   my ($ressREF, $carbsREF, $uniqNODEREF) = @_;
   my %ress = %{$ressREF};
   my %carbs = %{$carbsREF};
   my %uniqNODE = %{$uniqNODEREF};
   my %pre_nodes = ();
   my %change_nids = ();
   my $num = 1;

   foreach my $key (keys %carbs){
      my $monosaccharide = "";
      my $substi = $carbs{$key};
      if($substi =~ /(\d+)d\((.)\+(.)\)(\d+)n/){
         my $parent = $1;
         my $parentRES = $parent."b";
         my $parentBOND = $2;
         my $childBOND = $3;
         my $childRES = $4."s";

         my $saccharide = $ress{$parentRES}."||(".$parentBOND."d:".$childBOND.")".$ress{$childRES};

         if(exists $uniqNODE{$saccharide}){
            $monosaccharide = $uniqNODE{$saccharide};
            $pre_nodes{$num} = $monosaccharide;
            $change_nids{$parent} = $num;
            $num += 1;
         }else{
            my $url = "http://www.monosaccharidedb.org/convert_residue.action?sourceScheme=msdb&targetScheme=carbbank&name=$saccharide";
            ($http_response, $errorMessage) = &getHTTP($url);

            if($http_response =~ /<monosaccharide_name>(.+)<\/monosaccharide_name>/){
               $monosaccharide = $1;
		  
               if(length $monosaccharide != 0){
                  $pre_nodes{$num} = $monosaccharide;
                  $change_nids{$parent} = $num;
                  $num += 1;
                  $uniqNODE{$saccharide} = $monosaccharide;
                  print STDERR "STDERR 4) KCF NODE => $monosaccharide\nUnicarb NODE => $saccharide\nUnicarb RES ID => $key\n\n";
                  print STDERR "<input data> line around (before): $number\n";
               }else{
                  print STDERR "STDERR 5) *** Monosaccharide is EMPTY!! ***\n";
                  print STDERR "ERROR ---- $errorMessage\n\n";
                  print STDERR "<input data> line around (before): $number\n";
               }

            }elsif(length $errorMessage != 0){
               print STDERR "STDERR 6) ERROR at $saccharide ---- $errorMessage\n\n";
               print STDERR "<input data> line around (before): $number\n";
            }
         }

         delete $ress{$parentRES};
         delete $ress{$childRES};
      }
   }

   foreach my $key (keys %ress){
      my $monosaccharide = "";
      my $saccharide = $ress{$key};

      if($key =~ /(\d+)s/){
         $change_nids{$1} = $num;
         $pre_nodes{$num} = $uniqNODE{$saccharide};
         $num += 1;
      }elsif($key =~ /(\d+)b/ && exists $uniqNODE{$saccharide}){
         $parent = $1;
         $monosaccharide = $uniqNODE{$saccharide};

         $change_nids{$parent} = $num;
         $pre_nodes{$num} = $monosaccharide;
         $num += 1;
      }elsif($key =~ /(\d+)b/ && !(exists $uniqNODE{$saccharide})){
         $parent = $1;
         my $url = "http://www.monosaccharidedb.org/convert_residue.action?sourceScheme=msdb&targetScheme=carbbank&name=$saccharide";
         ($http_response, $errorMessage) = &getHTTP($url);

         if($http_response =~ /<monosaccharide_name>(.+)<\/monosaccharide_name>/){
            $monosaccharide = $1;
            if(length $monosaccharide != 0 and $key =~ /(\d+)\w/){
               $change_nids{$1} = $num;
               $pre_nodes{$num} = $monosaccharide;
               $num += 1;

               $uniqNODE{$saccharide} = $monosaccharide;
               print STDERR "STDERR 7) Now we are transforming Unicarb NODE => $saccharide\nUnicarb RES ID => $key\n";
               print STDERR "\tinto KCF NODE => $monosaccharide\n";
               print STDERR "<input data> line around (before): $number\n";
            }else{
               print STDERR "STDERR 8) *** Monosaccharide is EMPTY!! ***\n";
               print STDERR "ERROR ---- $errorMessage\n\n";
               print STDERR "<input data> line around (before): $number\n";
            }
         }elsif(length $errorMessage != 0){
            print STDERR "STDERR 9) ERROR at $saccharide ---- $errorMessage\n\n";
            print STDERR "<input data> line around (before): $number\n";
         }
      }elsif($key =~ /\d+[nra]/){
         print STDERR "STDERR 10) Check at $saccharide ---- There is another charactor than b and s\n\n";
         print STDERR "<input data> line around (before): $number\n";
      }
   }

   return(\%pre_nodes, \%change_nids, \%uniqNODE);
}





sub makeHashes4XY {
   my ($pre_nodesREF, $change_nidsREF, $linsREF, $number) = @_;
   my %pre_nodes = %{$pre_nodesREF}; 
   my %change_nids = %{$change_nidsREF};
   my %lins = %{$linsREF};
   my %pre_edges = ();
   my %nodedges = ();
   my %oyako = ();
   my %kooya = ();
   my $rootnode = -1;
   my $count = 0;

my $write_out;

   foreach my $key (sort keys %lins){
      my ($resP_num, $resC, $linP, $linC_num, $resP, $resC);
$write_out = $lins{$key};

      if($lins{$key} =~ /(\d+)o\(([\dx(-1)])\+([\dx(-1)])\)(\d+)[dn]/){
         $resP_num = $change_nids{$1};
         $linP = $2;
         $linC = $3;
         $resC_num = $change_nids{$4};

         $linP = " " if ($linP eq "x" || $linP == -1);
         $linC = " " if ($linC eq "x" || $linC == -1);

         $resC = $pre_nodes{$resC_num};
         if($resC =~ /^([ab])-([A-Z].*)$/){
            my $ab = $1;
            my $node = $2;
            $pre_nodes{$resC_num} = $node;
            my $edge = $resC_num.":".$ab.$linC."     ".$resP_num.":".$linP;

            if($linP eq " "){
               $nodedges{$resC_num} = 10;
            }else{
               $nodedges{$resC_num} = $linP;
            }

            $count += 1;
            $pre_edges{$count} = $edge;

         }elsif($resC =~ /^[A-Z].*$/){
            my $edge = $resC_num.":".$linC."     ".$resP_num.":".$linP;

            if($linP eq " "){
               $nodedges{$resC_num} = 10;
            }else{
               $nodedges{$resC_num} = $linP;
            }

            $count += 1;
            $pre_edges{$count} = $edge;
         }


         if(exists $oyako{$resP_num}){
            my @children = @{$oyako{$resP_num}};
            push(@children, $resC_num);
            $oyako{$resP_num} = [@children];
         }else{
            my @children = ($resC_num);
            $oyako{$resP_num} = [@children];
         }


         if(exists $kooya{$resC_num}){
            print STDERR "STDERR 11) Error: there are multi Parent for one child\n";
            print STDERR "\tchild: $pre_nodes{$resC_num}\n";
            print STDERR "\tparents: $pre_nodes{$resP_num} and $kooya{$resC_num}\n";
            print STDERR "<input data> line around (before): $number\n";
         }else{
            $kooya{$resC_num} = $resP_num;
         }
      }
   }

   my @oya = keys %oyako;
   my @ko = keys %kooya;
   my %tmp_list = ();

   foreach my $item (@ko){
      $tmp_list{$item} = 1;
   }
   foreach my $item (@oya){
      if(!(exists $tmp_list{$item})){
         $rootnode = $item;
      }
   }

   if($rootnode == -1){
      print STDERR "STDERR 12) Error: rootnode is -1\n";
      print STDERR "<input data> line around (before): $number\n";

      print STDERR "***$write_out\n";

      print STDERR "\n\@oya => ";
      foreach my $item_out (@oya){
         print STDERR "$item_out,\t";
      }
      print STDERR "\n\n\%tmp_list => ";
      foreach my $keys_out (keys (%tmp_list)){
         print STDERR "$keys_out => $tmp_list{$keys_out}\n";
      }
      print STDERR "\n\n";
   }

   return(\%pre_nodes, \%pre_edges, $rootnode, \%oyako, \%nodedges);
}




sub getX {
   my ($id, $oyakoRef, $nodesRef, $xHashRef, $habaHashRef) = @_;
   my %oyako = %{$oyakoRef};
   my %nodes = %{$nodesRef};
   my %xHash = %{$xHashRef};
   my %habaHash = %{$habaHashRef};
   my @children = ();
   if (exists $oyako{$id}) {
      @children = @{$oyako{$id}};
   }
   my $numChildren = @children;
   if ($numChildren == 0) {
      $habaHash{$id} = 0;
   }
   my $habaSum = 0;
   foreach my $child (@children) {
      $xHash{$child} = $xHash{$id} - 8;
      ($oyakoRef, $nodesRef, $xHashRef, $habaHashRef) = &getX($child,  \%oyako, \%nodes, \%xHash, \%habaHash);
      %oyako = %{$oyakoRef};
      %nodes = %{$nodesRef};
      %xHash = %{$xHashRef};
     %habaHash = %{$habaHashRef};
      $habaSum += $habaHash{$child};
   }
   if ($numChildren > 0) {
      $habaHash{$id} = ($numChildren-1) * 2 + $habaSum;
   }
   return (\%oyako, \%nodes, \%xHash, \%habaHash);
}


sub getY {
   my ($id, $oyakoRef, $nodesRef, $yHashRef, $habaHashRef, $nodedgeRef) = @_;
   my %oyako = %{$oyakoRef};
   my %nodes = %{$nodesRef};
   my %yHash = %{$yHashRef};
   my %habaHash = %{$habaHashRef};
   my %nodedges = %{$nodedgeRef};
   my @children = ();
   my %sortCh = ();
   if (defined $oyako{$id}) {
      @children = @{$oyako{$id}};
   }

   my $numChildren = @children;

   foreach my $chi (@children){
      $sortCh{$chi} = $nodedges{$chi};
   }
   my @soChi = sort{$sortCh{$a}<=>$sortCh{$b}} keys %sortCh;

   if ($numChildren == 1) {
      $yHash{$children[0]} = $yHash{$id};
   }elsif ($numChildren == 2) {
      $yHash{$soChi[0]} = $yHash{$id}+$habaHash{$id};
      $yHash{$soChi[1]} = $yHash{$id}-$habaHash{$id};
   }elsif ($numChildren == 3) {
      $yHash{$soChi[0]} = $yHash{$id}+$habaHash{$id};
      $yHash{$soChi[1]} = $yHash{$id};
      $yHash{$soChi[2]} = $yHash{$id}-$habaHash{$id};
   } elsif ($numChildren == 4) {
      $yHash{$soChi[0]} = $yHash{$id}+$habaHash{$id}+$habaHash{$id};
      $yHash{$soChi[1]} = $yHash{$id};
      $yHash{$soChi[2]} = $yHash{$id}-$habaHash{$id};
      $yHash{$soChi[3]} = $yHash{$id}-$habaHash{$id}-$habaHash{$id};
   } elsif ($numChildren == 5) {
      $yHash{$soChi[0]} = $yHash{$id}-$habaHash{$id}-$habaHash{$id};
      $yHash{$soChi[1]} = $yHash{$id}-$habaHash{$id};
      $yHash{$soChi[2]} = $yHash{$id};
      $yHash{$soChi[3]} = $yHash{$id}+$habaHash{$id};
      $yHash{$soChi[4]} = $yHash{$id}+$habaHash{$id}+$habaHash{$id};
   }

   foreach my $child (@children) {
      ($oyakoRef, $nodesRef, $yHashRef, $habaHashRef) = &getY($child, \%oyako, \%nodes, \%yHash, \%habaHash, \%nodedges);
      %oyako = %{$oyakoRef};
      %nodes = %{$nodesRef};
      %yHash = %{$yHashRef};
      %habaHash = %{$habaHashRef};
   }
   return (\%oyako, \%nodes, \%yHash, \%habaHash);
}



sub makeKCF {
   my ($rootnode, $nodesRef, $edgesRef, $xHashRef, $yHashRef, $numkcf) = @_;
   my %nodes = %{$nodesRef};
   my %edges = %{$edgesRef};
   my %xHash = %{$xHashRef};
   my %yHash = %{$yHashRef};
   my $result = "ENTRY         Uni-".$numkcf."             Glycan\n";
   my $numNodes = keys %nodes;
   $result .= "NODE  $numNodes\n";
   foreach my $key (sort {$a <=> $b} keys %nodes) {
      if($key == $rootnode && $nodes{$key} =~ /^[abox]-(.+)$/){
         $nodes{$key} = $1;
      }
         my $tmp_nd= $nodes{$key};
      $result .= "     ".$key."  ".$nodes{$key}."   ".$xHash{$key}."   ".$yHash{$key}."\n";
   }
   my $numEdges = keys %edges;
   $result .= "EDGE  $numEdges\n";
   foreach my $key (sort {$a <=> $b} keys %edges) {
      $result .= "     ".$key."  ".$edges{$key}."\n";
   }
   $result .= "///\n";
   return $result;
}




sub getHTTP{
        local $\ = "";

        my %arg = ( @_ );
        my $data;
        my ( $uri, $proxy_address, $proxy_port, $http_version, $user_id, $password,
                $proxy_user_id, $proxy_password, $referrer, $referer, $agent ) =
        (
                $arg{URL}           || $_[0],
                $arg{Proxy}         || "",
                $arg{ProxyPort}     || "8080",
                $arg{HTTP}          || "1.1",
                $arg{UserID}        || "",
                $arg{Password}      || "",
                $arg{ProxyUserID}   || "",
                $arg{ProxyPassword} || "",
                $arg{Referrer}      || "",
                $arg{Referer}       || "",
                $arg{UserAgent}     || "HTTP client version 1.02 (www.kasai.fm)"
        );

        my ( $scheme, $domain, $server_address, $server_port, $path );
        my ( $target_address, $target_port, $target_path, $target_ip );

######### URI parsing

        ### retrieve scheme ( "http://" )

        if ( $uri =~ s!^(\w+?)://!! ){
                $scheme = $1;

                return ("",
                        "Can't handle '$scheme'. Only 'http' is possible"
                ) if ( $scheme !~ /^http$/i );
        }
        else{
                $scheme = 'http';
        }


        ### retrieve domain, port and path ( "hogehoge:8080/foo/bar.html" )

        ( $domain, $path ) = split( /\//, $uri, 2 );
        ( $server_address, $server_port ) = split( /:/, $domain, 2 );

        $server_address ||= "localhost";
        $server_port    ||= getservbyname( $scheme, "tcp" );


        ### complete info about your proxy server
        if ( $proxy_address and !$proxy_port ){
                ( $proxy_port ) = $proxy_address =~ /:(\d+)/;

                return ("",
                        "Proxy port is undefined.".
                        "Specify 'ProxyPort'=>8080 or 'Proxy'=>'${proxy_address}:8080'"
                ) unless ( $proxy_port );
        }


        ### switch arguments according to if you use a proxy server

        ( $target_address, $target_port, $target_path ) = $proxy_address ?
                (
                        $proxy_address,
                        $proxy_port,
                        "${scheme}://${server_address}:${server_port}/${path}"
                ) :
                (
                        $server_address,
                        $server_port,
                        "/$path"
                );
 ######### SOCKET (create)

        $target_ip    = inet_aton( $target_address ) || return ("", "Can't connect to $target_address" );
        $sock_address = pack_sockaddr_in( $target_port, $target_ip );

        socket(SOCKET, PF_INET, SOCK_STREAM, 0) || return ("", "Can't create socket on $target_address");


 ######### SOCKET (connect)

        connect(SOCKET, $sock_address) or return ("", "Can't connect socket on $sock_address");
        autoflush SOCKET (1);


 ######### Send HTTP GET request

        if ( $http_version eq "1.1" ) {
                print SOCKET "GET $target_path HTTP/1.1\n";
                print SOCKET "Host: $target_address\n";
                print SOCKET "Connection: close\n";
        }
        else {
                print SOCKET "GET $target_path HTTP/1.0\n";
        }

        if ( $user_id ){
                print SOCKET "Authorization: Basic\n ";
                print SOCKET &base64'b64encode("${user_id}:${password}")."\n";
        }

        if ( $proxy_user_id ){
                print SOCKET "Proxy-Authorization: Basic\n ";
                print SOCKET &base64'b64encode("${proxy_user_id}:${proxy_password}")."\n";
        }
        print SOCKET "User-Agent: $agent\n"  if ( $agent    );
        print SOCKET "Referrer: $referrer\n" if ( $referrer );
        print SOCKET "Referer: $referer\n"   if ( $referer  );

        print SOCKET "Accept: text/html; */*\n";
        print SOCKET "\n";


        ######### Receive HTTP response via SOCKET

        while ( <SOCKET> ) {
                chomp;
                $data .= "$_\n";
        }


        ######### SOCKET (close); take down the session

        close(SOCKET);


        ######### return

        return ($data, "");
}















